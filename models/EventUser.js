var mongoose = require('mongoose');
const Schema = mongoose.Schema;

const EventUserSchema = new Schema({
    ttg_rev: { type: Number, default: 1 },
    name: { type: String },
    mid_name: { type: String },
    last_name: { type: String },
    email: { type: String },
    phone: { type: String },
    date_reg: { type: Date, default: Date.now },
    event_id: { type: String }
});

const EventUser = mongoose.model('EventUser', EventUserSchema);