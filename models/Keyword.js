var mongoose = require('mongoose');
const Schema = mongoose.Schema;

const KeywordSchema = new Schema({
    keyword: { type: String },
    event_ids: [String],
});

const Keyword = mongoose.model('Keyword', KeywordSchema);