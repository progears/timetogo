const mongoose = require('mongoose');
const random = require('mongoose-simple-random');
const Schema = mongoose.Schema;

const EventSchema = new Schema({
    ttg_rev: { type: Number },
    name: { type: String },
    short_description: { type: String },
    description: { type: String },
    keywords: [String],
    photos: [String],
    coordinates: [Number],
    create_date: { type: Date, default: Date.now },
    show: { type: Number, default: 0 },
    start_date: { type: Date },
    end_date: { type: Date },
    organizer_name: { type: String },
    phone: { type: String },
    category_id: { type: String }
});
EventSchema.plugin(random);

const Event = mongoose.model('Event', EventSchema);