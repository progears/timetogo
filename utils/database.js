"use strict";

exports.setUpConnection = setUpConnection;
exports.listEvents = listEvents;
exports.listRandomEvents = listRandomEvents;
exports.listUsers = listUsers;
exports.listCategories = listCategories;
exports.createUser = createUser;
exports.getUsersByEventId = getUsersByEventId;
exports.listLastEvents = listLastEvents;
exports.getEvent = getEvent;
exports.createEvent = createEvent;
exports.addKeyword = addKeyword;
exports.removeEvent = removeEvent;

var mongoose = require('mongoose');

require('../models/Category');
require('../models/Event');
require('../models/EventUser');
require('../models/Keyword');
const privateConfig = require('../private_config');

const Category = mongoose.model('Category');
const Event = mongoose.model('Event');
const EventUser = mongoose.model('EventUser');
const Keyword = mongoose.model('Keyword');

var mongodb_connection_string = 'mongodb://gluxix:' + privateConfig.password() + '@ds135820.mlab.com:35820/event';

function setUpConnection() {
    mongoose.connect(mongodb_connection_string);
}

function listUsers() {
    return EventUser.find();
}

function listCategories() {
    return Category.find();
}

function createUser(data) {
    const user = new EventUser({
        name: data.name,
        mid_name: data.mid_name,
        last_name: data.last_name,
        email: data.email,
        phone: data.phone,
        event_id: data.event_id
    });

    return user.save();
}

function getUsersByEventId(eventId) {
    return EventUser.findOne({ event_id: eventId }, function(err, data) {
        console.log(data);
    });
}

function listEvents() {
    return Event.find().sort({ create_date: -1 });
}

function listLastEvents(lastNum) {
    return Event.find().sort({ create_date: -1 }).limit(parseInt(lastNum));
}

function listRandomEvents(limit, callback) {
    Event.findRandom({}, {}, {limit: limit}, function(err, results) {
        callback(err, results);
    });
}

function getEvent(id) {
    return Event.findById(id, function(err, event) {
        console.log(event);
    });
}

function createEvent(data) {
    var keywords = [];
    if (data.keywords) {
        keywords = data.keywords.split(',').map(function(keyword) {
            return keyword.trim();
        });
    }
    var coordinates = [];
    if (data.coordinates) {
        coordinates = data.coordinates.split(',').map(function(coordinate) {
            return coordinate.trim();
        });;
    }
    const event = new Event({
        ttg_rev: 1,
        name: data.name,
        short_description: data.short_description,
        description: data.description,
        keywords: keywords,
        photos: data.photos,
        coordinates: coordinates,
        start_date: data.start_date,
        end_date: data.end_date,
        organizer_name: data.organizer_name,
        phone: data.phone,
        category_id: data.category_id
    });

    return event.save();
}

function addKeyword(data, eventId) {
    const keyword = new Keyword({
        keyword: data,
        event_ids: []
    });

    Keyword.find({ keyword: data }, function(err, doc) {
        if (doc.length) {
            var new_event_ids = doc[0].event_ids;

            if (new_event_ids && new_event_ids.indexOf(eventId) === -1) {
                new_event_ids.push(eventId);
            } else {
                console.log("This item already exists");
            }

            Keyword.findOneAndUpdate(
                { keyword: data }, { $set: { event_ids: new_event_ids } }, { new: true },
                function(err, res) {}
            );
        } else {
            keyword.save();
        }
    });
}

function removeEvent(id) {
    return Event.findOneAndRemove({ _id: id }, function(err, res) {
        console.log(res);
    });
}