exports.listLastEvents = listLastEvents;
exports.listRandomEvents = listRandomEvents;
exports.listRecommendEvents = listRecommendEvents;

var axios = require('axios');

function listLastEvents(lastNum) {
    return axios.get('http://localhost:8080/api/events/' + lastNum);
}

function listRandomEvents(limit) {
	return axios.get('http://localhost:8080/api/rnd_events/' + limit);
}

function listRecommendEvents(limit) {
    return axios.get('http://localhost:8080/api/recommend_events/' + limit);
}