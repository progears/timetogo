var express = require('express');
var router = express.Router();
var db = require('../utils/database.js');

router.get('/api/users', function(req, res) {
    db.listUsers().then(function(data) {
        return res.send(data);
    });
});

router.post('/api/users', function(req, res) {
    db.createUser(req.body).then(function(data) {
        res.send(data);
    });
});

router.get('/api/users/:eventId', function(req, res) {
    db.getUsersByEventId(req.params.eventId).then(function(data) {
        return res.send(data);
    });
});

module.exports = router;