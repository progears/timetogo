const express = require('express');
const router = express.Router();
const db = require('../utils/database.js');

router.get('/new_event', function(req, res) {
    res.render('add_event');
});

router.get('/api/events', function(req, res) {
    db.listEvents().then(function(data) {
        return res.send(data);
    });
});

router.post('/api/events', function(req, res) {
    db.createEvent(req.body)
        .then(function(data) {
            var keywordArray = data.keywords;
            keywordArray.forEach(function(item, i, keywordArr) {
                db.addKeyword(item, data._id);
            });
            return res.send(data);
        });
});

router.get('/api/events/:last', function(req, res) {
    db.listLastEvents(req.params.last).then(function(data) {
        return res.send(data);
    });
});

router.get('/api/event/:id', function(req, res) {
    db.getEvent(req.params.id).then(function(data) {
        return res.send(data);
    });
});

router.delete('/api/event/:id', function(req, res) {
    db.removeEvent(req.params.id).then(function(data) {
        res.send(data);
    });
});

router.get('/api/rnd_events/:limit', function(req, res) {
    db.listRandomEvents(req.params.limit, function(err, results) {
        if (!err) {
            return res.send(results);
        }
    });
});

router.get('/api/recommend_events/:limit', function(req, res) {
    //STUB
    db.listRandomEvents(req.params.limit, function(err, results) {
        if (!err) {
            return res.send(results);
        }
    });
    //TODO: real impl
});

module.exports = router;