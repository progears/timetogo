var express = require('express');
var router = express.Router();
var api = require('../utils/api.js');

router.get('/', function(req, res) {
	//TODO: !!!do it async!!!
	api.listLastEvents(3).then(function(interestingEvents) {
	    interestingEvents.data = interestingEvents.data || [];
		api.listRandomEvents(3).then(function(popularEvents) {
		    popularEvents.data = popularEvents.data || [];
            api.listRecommendEvents(3).then(function(recommendEvents) {
                recommendEvents.data = recommendEvents.data || [];
                res.render('index', {
                    "events": interestingEvents.data,
                    "rnd_events": popularEvents.data,
                    "recommend_events": recommendEvents.data
                });
            });
		});
	});
});

module.exports = router;