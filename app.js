const express = require('express');
const bodyParser = require('body-parser');
const db = require('./utils/database.js');

const app = express();
const server_port = process.env.PORT || 8080;

db.setUpConnection();
app.set('view engine', 'ejs');
app.set('views', 'views');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(require('./routes/events'));
app.use(require('./routes/users'));
app.use(require('./routes/categories'));
app.use(require('./routes/index'));

app.use(express.static('public'));

app.listen(server_port);